import { TICK, RESET } from './actionTypes';

// Action creators
export function doTick() {
	return {
		type: TICK
	};
}

export function doReset() {
	return {
		type: RESET
	};
}
