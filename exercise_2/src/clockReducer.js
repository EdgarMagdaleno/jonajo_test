import { TICK, RESET } from './actionTypes';

const initialState = {
	tick: 10
};

const makeClock = (state = initialState, action) => {
	console.log("ACTION: ", action.type)

	switch (action.type) {
		case TICK:
			return Object.assign({}, state, {
				tick: state.tick - 1
			});
		case RESET:
			return Object.assign({}, state, initialState);
		default:
			return state;
	}
}

export default makeClock;