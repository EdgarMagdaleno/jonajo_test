import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux'
import makeClock from './clockReducer';
import { createStore } from 'redux'
import App from './App';

import './assets/button.css';
import './assets/index.css';

const store = createStore(makeClock);

render(
	<Provider store={store}>
    	<App />
    </Provider>,
	document.getElementById('root')
);