import React, { Component } from 'react'
import { connect } from 'react-redux';
import { doTick, doReset } from './clockActions';

class App extends Component {
	constructor(props) {
		super(props);
		this.timer = null;

		this.state = {
			timer: null
		};

		this.reset = this.reset.bind(this);
		this.queueTick = this.queueTick.bind(this);
	}

	queueTick() {
		clearTimeout(this.timer);
		this.timer = setTimeout(() => this.props.dispatch(doTick()), 1000);
	}

	reset() {
		this.props.dispatch(doReset());
		this.queueTick();
	}

	render() {
		const { tick } = this.props;

		if (tick > 0) {
			this.queueTick();
		}

		return (
			<>
				<h1>{tick > 0 ? tick : 'Game Over'}</h1>
				<button onClick={this.reset} className="green button"><i className="material-icons">wifi_protected_setup</i>RESET</button>
			</>
		);
	}
}

function mapStateToProps(state) {
	return {
		tick: state.tick
	}
};

export default connect(mapStateToProps)(App);