import React, { Component } from 'react'

const LoadingData = (WrappedComponent, dataSource) => {
	return class extends Component {
		constructor(props) {
			super(props);

			this.state = {
				loading: true,
				dataSource
			};
		}

		componentDidMount() {
			const { dataSource } = this.state;
			console.log("dataSource: ", dataSource);

			fetch(dataSource)
			.then(response => response.json())
			.then(obj => obj.results)
			.then(data => this.setState({
				loading: false,
				data
			}));
		}

		render() {
			const { loading, data } = this.state;

			if (loading) return (<img src="loading.gif" alt="loading" />);
			return (<WrappedComponent data={data} {...this.props} />)
		}
	}
}

export default LoadingData;