import React, { Component } from 'react'

class UsersList extends Component {
	render() {
		const { data } = this.props;
		console.log(data);

		return (
			<div className="grid">
				{ data.map((person, i) => (
					<div key={i}>
						<p>{person.name.first} {person.name.last}</p>
						<img src={person.picture.large} alt={person.name.last} />
					</div>
				))}
			</div>
		);
	}
}

export default UsersList;