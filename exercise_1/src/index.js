import React from 'react';
import { render } from 'react-dom';
import UsersList from './UsersList';
import LoadingData from './LoadingData'

import './index.css';

const dataSource = 'https://randomuser.me/api/?results=10';
const UsersListWithLoading = LoadingData(UsersList, dataSource);

render(<UsersListWithLoading />, document.getElementById('root'));
